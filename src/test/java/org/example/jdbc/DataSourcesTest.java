package org.example.jdbc;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;

import java.util.EnumMap;
import java.util.Map;
import java.util.Properties;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.assertThatExceptionOfType;
import static org.junit.jupiter.api.Assertions.fail;

/**
 * Requirements :
 * <ul>
 *     <li><b>Docker engine</b>, as it is needed by TestContainers</li>
 * </ul>
 * For Testcontainers + JUnit 5 see <a href="https://www.testcontainers.org/test_framework_integration/junit_5">this link</a>
 */
@Testcontainers
class DataSourcesTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataSourcesTest.class);

    private static final String MAIN_DB_DATABASE_NAME = "test-db";
    private static final String MAIN_DB_USERNAME = "test";
    private static final String MAIN_DB_PASSWORD = "test123";

    @Container
    private static PostgreSQLContainer<?> postgreSQLContainer
            = new PostgreSQLContainer<>(DockerImageName.parse(PostgreSQLContainer.IMAGE).withTag("16"))
            .withDatabaseName(MAIN_DB_DATABASE_NAME)
            .withUsername(MAIN_DB_USERNAME)
            .withPassword(MAIN_DB_PASSWORD);

    @BeforeAll
    static void setUpGlobal() throws Exception {
        LOGGER.info("Host : {}", postgreSQLContainer.getHost());
        LOGGER.info("Mapped port for 5432 : {}", postgreSQLContainer.getMappedPort(PostgreSQLContainer.POSTGRESQL_PORT));
    }

    @Test
    void initAll() {
        try {
            Properties overridePropertiesMainDs = new Properties();
            overridePropertiesMainDs.put("dataSource.serverName", postgreSQLContainer.getHost());
            overridePropertiesMainDs.put("dataSource.portNumber", postgreSQLContainer.getMappedPort(PostgreSQLContainer.POSTGRESQL_PORT));
            overridePropertiesMainDs.put("dataSource.databaseName", MAIN_DB_DATABASE_NAME);
            overridePropertiesMainDs.put("dataSource.user", MAIN_DB_USERNAME);
            overridePropertiesMainDs.put("dataSource.password", MAIN_DB_PASSWORD);

            Map<DataSources.DataSourceInstance, Properties> overridePropertiesMap
                    = new EnumMap<>(DataSources.DataSourceInstance.class);
            overridePropertiesMap.put(DataSources.DataSourceInstance.MAIN, overridePropertiesMainDs);

            DataSources.initAll(overridePropertiesMap);
            assertThat(DataSources.getDataSourceInstance(DataSources.DataSourceInstance.MAIN)).isNotNull();
        } catch (Exception ex) {
            LOGGER.error(ex.getMessage(), ex);
            fail("Should not throw : " + ex.toString());
        }
    }

    @Test
    void shutdownAll() {
        Properties overridePropertiesMainDs = new Properties();
        overridePropertiesMainDs.put("dataSource.serverName", postgreSQLContainer.getHost());
        overridePropertiesMainDs.put("dataSource.portNumber", postgreSQLContainer.getMappedPort(5432));
        overridePropertiesMainDs.put("dataSource.databaseName", MAIN_DB_DATABASE_NAME);
        overridePropertiesMainDs.put("dataSource.user", MAIN_DB_USERNAME);
        overridePropertiesMainDs.put("dataSource.password", MAIN_DB_PASSWORD);

        Map<DataSources.DataSourceInstance, Properties> overridePropertiesMap
                = new EnumMap<>(DataSources.DataSourceInstance.class);
        overridePropertiesMap.put(DataSources.DataSourceInstance.MAIN, overridePropertiesMainDs);

        DataSources.initAll(overridePropertiesMap);
        DataSources.shutdownAll();
        assertThatExceptionOfType(IllegalStateException.class)
                .isThrownBy(() -> DataSources.getDataSourceInstance(DataSources.DataSourceInstance.MAIN))
                .withMessage("DataSources not initialized.");
    }

    @Test
    void getDataSourceWithoutInitialization() {
        assertThatExceptionOfType(IllegalStateException.class)
                .isThrownBy(() -> DataSources.getDataSourceInstance(DataSources.DataSourceInstance.MAIN))
                .withMessage("DataSources not initialized.");
    }

}