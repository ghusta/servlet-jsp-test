package org.example.webjars;

import org.example.junit.ReplaceCamelCaseWordsAndUnderscores;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.webjars.WebJarVersionLocator.WEBJARS_PATH_PREFIX;

/**
 * See also in <i>spring-webmvc</i> :
 * <code>org.springframework.web.servlet.resource.WebJarsResourceResolver</code>
 */
@DisplayNameGeneration(ReplaceCamelCaseWordsAndUnderscores.class)
class WebJarAssetLocatorTest {

    private static String bootstrapVersion;

    @BeforeAll
    static void setUp() throws Exception {
        // find metadata in bootstrap maven dependency
        findDependencyBootstrap();
    }

    private static void findDependencyBootstrap() {
        bootstrapVersion = WebJarAssetLocatorHelper.webJarVersion("bootstrap").orElseThrow();
    }

    @Test
    void containsWebJar_Bootstrap() {
        assertThat(bootstrapVersion).isNotEmpty();
    }

    @Test
    void containsAssetsForBootstrap() {
        assertThat(WebJarAssetLocatorHelper.fullPath("bootstrap", "/css/bootstrap.css")).isNotEmpty();
        assertThat(WebJarAssetLocatorHelper.fullPath("bootstrap", "css/bootstrap.min.css")).isNotEmpty();
    }

    @Test
    void resolveAssetInWebJarBootstrapVersionAgnostic() {
        String webjar = "bootstrap";
        String partialPath = "css/bootstrap.min.css";
        // version-agnostic locator
        String webJarPath = WebJarAssetLocatorHelper.fullPath(webjar, partialPath).orElseThrow();
        assertThat(webJarPath).startsWith(WEBJARS_PATH_PREFIX);
    }

    @Test
    @Disabled("not checked anymore in webjars-locator-lite v1.0.0")
    void resolveUnknownAsset() {
        String webjar = "bootstrap";
        String partialPath = "css/boooooootstrap.css";
        assertThat(WebJarAssetLocatorHelper.fullPath(webjar, partialPath)).isEmpty();
    }

    @Test
    void findWebJarResourcePath() {
        String webjar = "bootstrap";
        String partialPath = "css/bootstrap.min.css";
        String webJarResourcePath = WebJarAssetLocatorHelper.fullPath(webjar, partialPath).orElseThrow();
        assertThat(webJarResourcePath)
                .isEqualTo(String.format("%s/bootstrap/%s/css/bootstrap.min.css", WEBJARS_PATH_PREFIX, bootstrapVersion));
    }

}
