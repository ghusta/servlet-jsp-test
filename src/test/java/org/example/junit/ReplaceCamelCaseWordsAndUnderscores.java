package org.example.junit;

import org.junit.jupiter.api.DisplayNameGenerator;

import java.lang.reflect.Method;

public class ReplaceCamelCaseWordsAndUnderscores extends DisplayNameGenerator.Simple {

    @Override
    public String generateDisplayNameForMethod(Class<?> testClass, Method testMethod) {
        return parseString(super.generateDisplayNameForMethod(testClass, testMethod));
    }

    private String parseString(String generateDisplayNameForMethod) {
        StringBuilder res = new StringBuilder();
        for (int i = 0; i < generateDisplayNameForMethod.length(); i++) {
            if (Character.isUpperCase(generateDisplayNameForMethod.charAt(i))) {
                if (i != 0) {
                    res.append(" ");
                }
                res.append(Character.toLowerCase(generateDisplayNameForMethod.charAt(i)));
            } else if (generateDisplayNameForMethod.charAt(i) == '_') {
                res.append(" ");
            } else {
                res.append(generateDisplayNameForMethod.charAt(i));
            }
        }
        return res.toString();
    }

}
