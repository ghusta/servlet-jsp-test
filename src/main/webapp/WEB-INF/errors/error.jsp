<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ page isErrorPage = "true" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="/webjars-tags" prefix="webjars" %>
<%
    // log to server with stacktrace
    log(exception.getMessage(), exception);
    log("Cause exception : " + exception.getCause());
    log("Classe exception : " + exception.getClass());

    pageContext.setAttribute("ctx", request.getContextPath());
%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Error</title>
    <base href="${ctx}/">
    <%-- Assets for Bootstrap --%>
    <link rel="stylesheet" href="<webjars:asset webjar='bootstrap' path='css/bootstrap.min.css' />" />
</head>
<body>
<div class="container">
    <div class="alert alert-danger" role="alert">
        <h2 class="alert-heading">An error occured</h2>
    </div>
    <div>
    <strong>Exception is:</strong>
        <div>
        <textarea rows="8" cols="80" readonly>
<%= exception %>
        </textarea>
        </div>
    </div>
    <br>
    <br>
    <div>
    <strong>Exception message is:</strong>
        <div>
        <textarea rows="8" cols="80" readonly>
<%= exception.getMessage() %>
        </textarea>
        </div>
    </div>
    <br>
    <br>
    <div>
    <strong>Exception cause:</strong>
        <pre>
<%= exception.getCause() %>
        </pre>
    </div>
    <br>
    <br>
    <div>
    <strong>Exception class:</strong>
        <pre>
<%= exception.getClass() %>
        </pre>
    </div>
</div>
</body>
</html>
