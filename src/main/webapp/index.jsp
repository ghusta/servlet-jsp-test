<%@ page language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="/webjars-tags" prefix="webjars" %>
<%
    pageContext.setAttribute("ctx", request.getContextPath());
%>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Example</title>
    <link href="favicon.ico" rel="icon" type="image/x-icon" />
    <base href="${ctx}/">
    <%-- Assets for Bootstrap --%>
    <link rel="stylesheet" href="<webjars:asset webjar='bootstrap' path='css/bootstrap.min.css' />" />
    <%-- Assets for Font-Awesome --%>
    <link rel="stylesheet" href="<webjars:asset webjar='font-awesome' path='css/fontawesome.min.css' />" />
    <link rel="stylesheet" href="<webjars:asset webjar='font-awesome' path='css/brands.min.css' />" />
    <link rel="stylesheet" href="<webjars:asset webjar='font-awesome' path='css/solid.min.css' />" />
</head>
<body>
<div class="container">
    <h2><i class="fas fa-rocket fa-fw"></i> Hello World!</h2>
    <ul>
        <li><a href="test.jsp"><i class="fas fa-exclamation-triangle fa-fw text-warning"></i> Go to test.jsp : ERR</a></li>
        <li><a href="test.jsp?a=6&b=2"><i class="fas fa-check fa-fw text-success"></i> Go to test.jsp : OK</a></li>
        <li><a href="test.jsp?a=1&b=0"><i class="fas fa-bomb fa-fw text-dark"></i> Go to test.jsp : KO</a></li>
    </ul>
    <br/>
    <h2><i class="fas fa-bug fa-fw"></i> Simulate errors</h2>
    <ul>
        <li><a href="test/test-1.jsp">Generate <code>java.lang.Exception</code></a></li>
    </ul>
    <br/>
    <h2><i class="fas fa-database fa-fw"></i> DataSources</h2>
    <ul>
        <li><a href="test-jdbc">Test query on DataSource 'main'</a></li>
    </ul>
    <br/>
    <h2><i class="fas fa-gauge-high fa-fw"></i> Metrics</h2>
    <ul>
        <li><a href="metrics">Prometheus Metrics</a></li>
    </ul>
</div>
</body>
</html>
