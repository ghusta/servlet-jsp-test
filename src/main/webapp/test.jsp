<%@ page language="java" %>
<%@ page trimDirectiveWhitespaces="true" %>
<%@ taglib uri="/webjars-tags" prefix="webjars" %>
<%@ page import="org.example.Utils" %>
<%
    String paramA = request.getParameter("a");
    String paramB = request.getParameter("b");

%>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Test</title>
    <%-- Assets for Bootstrap --%>
    <link rel="stylesheet" href="<webjars:asset webjar='bootstrap' path='css/bootstrap.min.css' />" />
</head>
<body>
<div class="container">
    <h2>Test</h2>
    <h3>Calculation : </h3>
    <div>
    Param a = <%= paramA %>
    </div>
    <div>
    Param b = <%= paramB %>
    </div>
    <div>
    a / b = <%= Utils.divide(paramA, paramB) %>
    </div>
</div>
</body>
</html>
