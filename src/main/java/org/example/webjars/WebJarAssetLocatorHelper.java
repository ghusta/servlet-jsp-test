package org.example.webjars;

import org.webjars.WebJarVersionLocator;

import java.util.Optional;

/**
 * Inspired by <code>org.springframework.web.servlet.resource.WebJarsResourceResolver</code> (spring-webmvc).
 */
public class WebJarAssetLocatorHelper {

    public static final String WEBJARS_PATH_PREFIX = WebJarVersionLocator.WEBJARS_PATH_PREFIX;

    private static final WebJarVersionLocator webJarVersionLocator;

    static {
        webJarVersionLocator = new WebJarVersionLocator();
    }

    private WebJarAssetLocatorHelper() {
    }

    /**
     * Path starts with {@link #WEBJARS_PATH_PREFIX}.
     *
     * @see WebJarVersionLocator#fullPath(String, String)
     */
    public static Optional<String> fullPath(final String webJarName, final String exactPath) {
        String localExactPath = exactPath;
        if (exactPath.startsWith("/")) {
            localExactPath = exactPath.substring(1);
        }
        return Optional.ofNullable(webJarVersionLocator.fullPath(webJarName, localExactPath));
    }

    public static Optional<String> webJarVersion(String webJarName) {
        return Optional.ofNullable(webJarVersionLocator.version(webJarName));
    }

}
