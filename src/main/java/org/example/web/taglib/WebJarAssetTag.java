package org.example.web.taglib;

import jakarta.servlet.jsp.JspException;
import jakarta.servlet.jsp.JspTagException;
import jakarta.servlet.jsp.tagext.TagSupport;
import org.example.webjars.WebJarAssetLocatorHelper;

import static org.example.webjars.WebJarAssetLocatorHelper.WEBJARS_PATH_PREFIX;

/**
 * Renders the asset full path, including the context path, for a given WebJar.
 */
public class WebJarAssetTag extends TagSupport {

    public static final String WEBJAR_ASSET_PREFIX = "/webjars";

    private String webjar;
    private String path;

    public void setWebjar(String webjar) {
        this.webjar = webjar;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public WebJarAssetTag() {
        super();
    }

    @Override
    public int doEndTag() throws JspException {
        String webJarResourcePath = WebJarAssetLocatorHelper.fullPath(webjar, path).orElse(null);
        String result = null;
        if (webJarResourcePath != null) {
            String contextPath = pageContext.getServletContext().getContextPath();
            result = contextPath + WEBJAR_ASSET_PREFIX + webJarResourcePath.replace(WEBJARS_PATH_PREFIX, "");
        }

        if (result != null) {
            try {
                pageContext.getOut().print(result);
            } catch (java.io.IOException ex) {
                throw new JspTagException(ex.toString(), ex);
            }
        }

        return EVAL_PAGE;
    }

    @Override
    public void release() {
        webjar = null;
        path = null;
        super.release();
    }
}
