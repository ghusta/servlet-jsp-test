package org.example.web.servlet;

import io.micrometer.core.instrument.Clock;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.Tags;
import io.micrometer.prometheusmetrics.PrometheusConfig;
import io.micrometer.prometheusmetrics.PrometheusMeterRegistry;
import io.prometheus.metrics.model.registry.PrometheusRegistry;
import jakarta.servlet.ServletContext;
import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.example.metrics.MetricsInitializer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * Initialization of metrics registry.
 */
@WebListener
public class StartupListener implements ServletContextListener {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("Micrometer metrics initialization...");
        ServletContext servletContext = sce.getServletContext();
        MetricsInitializer metricsInitializer = new MetricsInitializer();
        PrometheusMeterRegistry prometheusMeterRegistry
                = new PrometheusMeterRegistry(PrometheusConfig.DEFAULT, PrometheusRegistry.defaultRegistry, Clock.SYSTEM);
        // Add common tag "application" for Grafana dashboard id=4701
        // See : https://micrometer.io/docs/registry/prometheus#_grafana_dashboard
        // and : https://grafana.com/dashboards/4701
        // and : https://docs.spring.io/spring-boot/docs/current/reference/html/production-ready-features.html#production-ready-metrics-getting-started
        prometheusMeterRegistry.config()
                .commonTags(Tags.of(Tag.of("application", "my-demo-webapp")));

        List<MeterRegistry> registries = List.of(prometheusMeterRegistry);
        metricsInitializer.initialize(registries, servletContext);
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        Metrics.globalRegistry.close();
    }

}
