package org.example.web.servlet;

import jakarta.servlet.ServletException;
import jakarta.servlet.annotation.WebServlet;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.example.jdbc.DataSources;

import javax.sql.DataSource;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

@WebServlet(urlPatterns = {"/test-jdbc"})
public class TestJdbcSvt extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PrintWriter writer = response.getWriter();

        Connection cnx = null;
        try {
            // get a connection
            DataSource dataSourceMain = DataSources.getDataSourceInstance(DataSources.DataSourceInstance.MAIN);
            cnx = dataSourceMain.getConnection();

            // execute simple query
            try (Statement stmt = cnx.createStatement()) {
                ResultSet resultSet = stmt.executeQuery("SELECT 1");
                resultSet.close();
            }

            // print result
            response.setContentType("text/plain");
            writer.println("SQL Request executed");
            response.flushBuffer();
        } catch (SQLException ex) {
            response.sendError(HttpServletResponse.SC_INTERNAL_SERVER_ERROR, ex.getMessage());
            response.flushBuffer();
        } finally {
            if (cnx != null) {
                try {
                    cnx.close();
                } catch (SQLException throwables) {
                    // NOP
                }
            }
        }
    }

}
