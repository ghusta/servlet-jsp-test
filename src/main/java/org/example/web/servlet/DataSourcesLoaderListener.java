package org.example.web.servlet;

import jakarta.servlet.ServletContextEvent;
import jakarta.servlet.ServletContextListener;
import jakarta.servlet.annotation.WebListener;
import org.example.jdbc.DataSources;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@WebListener
public class DataSourcesLoaderListener implements ServletContextListener {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        log.info("DataSources initialization...");
        DataSources.initAll();
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        log.info("DataSources shutdown...");
        DataSources.shutdownAll();
    }

}
