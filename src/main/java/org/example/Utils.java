package org.example;

public class Utils {

    public static int divide(String paramA, String paramB) {
        int res = 0;
        try {
            res = Integer.parseInt(paramA) / Integer.parseInt(paramB);
        } catch (NumberFormatException e) {
            throw new RuntimeException(e);
        }
        return res;
    }

}
