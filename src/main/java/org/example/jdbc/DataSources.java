package org.example.jdbc;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import com.zaxxer.hikari.metrics.MetricsTrackerFactory;
import com.zaxxer.hikari.metrics.micrometer.MicrometerMetricsTrackerFactory;
import com.zaxxer.hikari.metrics.prometheus.PrometheusMetricsTrackerFactory;
import com.zaxxer.hikari.util.PropertyElf;
import io.micrometer.core.instrument.Metrics;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.Closeable;
import java.io.IOException;
import java.util.EnumMap;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.atomic.AtomicBoolean;

public class DataSources {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataSources.class);

    /**
     * See {@link HikariConfig#setRegisterMbeans(boolean)}
     */
    private static final boolean REGISTER_MBEANS = true;

    /**
     * See {@link HikariConfig#setMetricsTrackerFactory(MetricsTrackerFactory)}
     */
    @Deprecated
    private static final boolean ENABLE_PROMETHEUS_METRICS = false;

    private static final boolean ENABLE_MICROMETER_METRICS = true;

    private static final AtomicBoolean initializedAll = new AtomicBoolean(false);

    public enum DataSourceInstance {
        MAIN(),
        ARCHIVE(false);

        private final boolean enabled;

        DataSourceInstance() {
            this(true);
        }

        DataSourceInstance(boolean enabled) {
            this.enabled = enabled;
        }
    }

    private static final EnumMap<DataSourceInstance, DataSource> dataSourceInstances
            = new EnumMap<>(DataSourceInstance.class);

    public static DataSource getDataSourceInstance(DataSourceInstance dataSourceInstance) {
        if (!initializedAll.get()) {
            throw new IllegalStateException("DataSources not initialized.");
        }
        return dataSourceInstances.get(dataSourceInstance);
    }

    private DataSources() {
    }

    public static synchronized void initAll() {
        initAll(null);
    }

    /**
     * Method protected, almost for tests.
     *
     * @param overridePropertiesMap Properties to override after reading the properties file
     */
    protected static synchronized void initAll(Map<DataSourceInstance, Properties> overridePropertiesMap) {
        if (initializedAll.compareAndSet(false, true)) {
            for (DataSourceInstance instance : DataSourceInstance.values()) {
                if (instance.enabled) {
                    DataSource ds = initDs(instance,
                            overridePropertiesMap == null ? null : overridePropertiesMap.get(instance));
                    dataSourceInstances.put(instance, ds);
                }
            }
        }
    }

    private static DataSource initDs(DataSourceInstance dataSourceInstance) {
        return initDs(dataSourceInstance, null);
    }

    private static DataSource initDs(DataSourceInstance dataSourceInstance, Properties overrideProperties) {
        String instanceName = dataSourceInstance.name().toLowerCase();
        LOGGER.debug("Initialization in progress for DataSource '{}'...", instanceName);
        // See : https://github.com/brettwooldridge/HikariCP#initialization
        // Search properties file in FS then in classpath
        HikariConfig config = new HikariConfig(String.format("/hikari-%s.properties", instanceName));
        if (overrideProperties != null) {
            // config.setDataSourceProperties(overrideProperties);
            PropertyElf.setTargetFromProperties(config, overrideProperties);
        }
        // Register JMX MBeans : HikariConfigMXBean / HikariPoolMXBean
        config.setRegisterMbeans(REGISTER_MBEANS);

        // Metrics
        if (ENABLE_PROMETHEUS_METRICS) {
            config.setMetricsTrackerFactory(new PrometheusMetricsTrackerFactory());
        }
        if (ENABLE_MICROMETER_METRICS) {
            if (config.getMetricsTrackerFactory() != null) {
                throw new IllegalStateException("HikariConfig's metricsTrackerFactory already set");
            }
            config.setMetricsTrackerFactory(new MicrometerMetricsTrackerFactory(Metrics.globalRegistry));
        }

        return new HikariDataSource(config);
    }

    public static synchronized void shutdownAll() {
        if (initializedAll.compareAndSet(true, false)) {
            for (DataSourceInstance instance : DataSourceInstance.values()) {
                if (instance.enabled) {
                    shutdownDs(instance);
                    dataSourceInstances.remove(instance);
                }
            }
        }
    }

    private static void shutdownDs(DataSourceInstance dataSourceInstance) {
        DataSource ds = dataSourceInstances.get(dataSourceInstance);
        if (ds instanceof Closeable) {
            try {
                LOGGER.debug("Shutdown in progress for DataSource '{}'...", dataSourceInstance.name().toLowerCase());
                ((Closeable) ds).close();
            } catch (IOException e) {
                LOGGER.error(e.getMessage());
            }
        }
    }

}
