package org.example.metrics;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tags;
import io.micrometer.core.instrument.binder.MeterBinder;
import io.micrometer.core.instrument.binder.jvm.ClassLoaderMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmGcMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmInfoMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmMemoryMetrics;
import io.micrometer.core.instrument.binder.jvm.JvmThreadMetrics;
import io.micrometer.core.instrument.binder.system.FileDescriptorMetrics;
import io.micrometer.core.instrument.binder.system.ProcessorMetrics;
import io.micrometer.core.instrument.binder.system.UptimeMetrics;
import io.micrometer.core.instrument.binder.tomcat.TomcatMetrics;
import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import jakarta.servlet.ServletContext;
import org.apache.catalina.Manager;
import org.apache.catalina.core.ApplicationContext;
import org.apache.catalina.core.ApplicationContextFacade;
import org.apache.catalina.core.StandardContext;
import org.example.metrics.binder.jvm.CustomJvmMemoryMetrics;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

/**
 * Micrometer metrics initializer.
 * <p>
 * See : org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryPostProcessor
 */
public class MetricsInitializer {

    private final List<MeterBinder> meterBinders;

    public MetricsInitializer() {
        this.meterBinders = new ArrayList<>();
    }

    /**
     * Should provide MeterRegistry implementations.
     *
     * @param registries     MeterRegistry implementations
     * @param servletContext
     */
    public void initialize(List<MeterRegistry> registries, ServletContext servletContext) {
        // see also : https://micrometer.io/docs/ref/jvm
        initJvmMetrics();
        initSystemMetrics();
        try {
            initServerMetrics(servletContext);
        } catch (NoSuchFieldException | IllegalAccessException e) {
            throw new RuntimeException(e);
        }

        boolean hasCompositeMeterRegistry = false;
        for (MeterRegistry registry : registries) {
            if (registry instanceof CompositeMeterRegistry) {
                hasCompositeMeterRegistry = true;
                break;
            }
        }

        MeterRegistryConfigurer meterRegistryConfigurer = new MeterRegistryConfigurer(meterBinders, true, hasCompositeMeterRegistry);
        registries.forEach(meterRegistryConfigurer::configure);
    }

    /**
     * See org.springframework.boot.actuate.autoconfigure.metrics.JvmMetricsAutoConfiguration
     */
    private void initJvmMetrics() {
        this.meterBinders.add(new JvmInfoMetrics());
        this.meterBinders.add(new JvmGcMetrics());
        this.meterBinders.add(new JvmMemoryMetrics());
        this.meterBinders.add(new CustomJvmMemoryMetrics());
        this.meterBinders.add(new JvmThreadMetrics());
        this.meterBinders.add(new ClassLoaderMetrics());
    }

    /**
     * See org.springframework.boot.actuate.autoconfigure.metrics.SystemMetricsAutoConfiguration
     */
    private void initSystemMetrics() {
        this.meterBinders.add(new UptimeMetrics());
        this.meterBinders.add(new ProcessorMetrics());
        this.meterBinders.add(new FileDescriptorMetrics());
    }

    private void initServerMetrics(ServletContext servletContext) throws NoSuchFieldException, IllegalAccessException {
        ApplicationContextFacade acf = (ApplicationContextFacade) servletContext;

        Field applicationContextFacadeField = ApplicationContextFacade.class.getDeclaredField("context");
        applicationContextFacadeField.setAccessible(true);

        ApplicationContext appContext = (ApplicationContext) applicationContextFacadeField.get(acf);
        Field applicationContextField = ApplicationContext.class.getDeclaredField("context");
        applicationContextField.setAccessible(true);

        StandardContext stdContext;
        stdContext = (StandardContext) applicationContextField.get(appContext);

        Manager tomcatManager = stdContext.getManager();
        this.meterBinders.add(new TomcatMetrics(tomcatManager, Tags.empty()));
    }

}
