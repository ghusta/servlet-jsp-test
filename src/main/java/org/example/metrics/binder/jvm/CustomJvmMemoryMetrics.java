package org.example.metrics.binder.jvm;

import io.micrometer.common.lang.NonNullApi;
import io.micrometer.core.instrument.Gauge;
import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Tag;
import io.micrometer.core.instrument.binder.BaseUnits;
import io.micrometer.core.instrument.binder.MeterBinder;

import static java.util.Collections.emptyList;

@NonNullApi
public class CustomJvmMemoryMetrics implements MeterBinder {

    private final Iterable<Tag> tags;

    public CustomJvmMemoryMetrics() {
        this(emptyList());
    }

    public CustomJvmMemoryMetrics(Iterable<Tag> tags) {
        this.tags = tags;
    }

    @Override
    public void bindTo(MeterRegistry registry) {
        Runtime runtime = Runtime.getRuntime();
        // Should be equivalent to Maximum Heap Size (set by jvm arg -Xmx)
        Gauge.builder("custom.jvm.memory.max", runtime, Runtime::maxMemory)
                .tag("area", "heap")
                .tags(tags)
                .description("The maximum amount of memory that the virtual machine will attempt to use (java.lang.Runtime.maxMemory)")
                .baseUnit(BaseUnits.BYTES)
                .register(registry);

        Gauge.builder("custom.jvm.memory.free", runtime, Runtime::freeMemory)
                .tag("area", "heap")
                .tags(tags)
                .description("An approximation to the total amount of memory currently available for future allocated objects (java.lang.Runtime.freeMemory)")
                .baseUnit(BaseUnits.BYTES)
                .register(registry);
    }

}
