package org.example.metrics;

import io.micrometer.core.instrument.MeterRegistry;
import io.micrometer.core.instrument.Metrics;
import io.micrometer.core.instrument.binder.MeterBinder;
import io.micrometer.core.instrument.composite.CompositeMeterRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

/**
 * See : org.springframework.boot.actuate.autoconfigure.metrics.MeterRegistryConfigurer
 */
class MeterRegistryConfigurer {

    protected final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Micrometer's list of registered MeterBinder
     */
    private final List<MeterBinder> binders;

    private final boolean addToGlobalRegistry;

    private final boolean hasCompositeMeterRegistry;

    public MeterRegistryConfigurer(List<MeterBinder> meterBinders, boolean addToGlobalRegistry, boolean hasCompositeMeterRegistry) {
        this.binders = meterBinders;
        this.addToGlobalRegistry = addToGlobalRegistry;
        this.hasCompositeMeterRegistry = hasCompositeMeterRegistry;
    }

    void configure(MeterRegistry registry) {
        if (!this.hasCompositeMeterRegistry || registry instanceof CompositeMeterRegistry) {
            addBinders(registry);
        }
        if (this.addToGlobalRegistry && registry != Metrics.globalRegistry) {
            Metrics.addRegistry(registry);
        }
    }

    private void addBinders(MeterRegistry registry) {
        this.binders.forEach(binder -> binder.bindTo(registry));
    }

}
